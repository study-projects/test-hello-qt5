

# TODO

- [ ] translation
  https://doc.qt.io/qt-5/qtlinguist-hellotr-example.html

- [ ] AppVeyor settings

# Issue

- [x] build number management
  `git push origin master` will run AppVeyor build automatically,
  it will increment build number.
  - SOLUTION: use Pull Request

## AppVeyor

- https://www.appveyor.com/docs/packaging-artifacts/

## Win10 package eco system

- NuGet
- Chocolatey
- PowerShell
- ...
