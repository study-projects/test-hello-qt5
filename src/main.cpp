// SPDX-License-Identifier: MIT

#include <QtWidgets>

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	QMainWindow win;

	win.setWindowTitle(QObject::tr("Hello, Qt5!"));
	win.resize(400, 200);
	win.show();

	return app.exec();
}
